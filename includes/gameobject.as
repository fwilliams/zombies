
var PLAYER_L = "Bike/Bike_L.png";
var PLAYER_U = "Bike/Bike_U.png";
var PLAYER_D = "Bike/Bike_D.png";
var STEAM_U = "Steamroller/Steamroller_U.png";
var STEAM_D = "Steamroller/Steamroller_D.png";
var STEAM_L = "Steamroller/Steamroller_L.png";
var STEAM_R = "Steamroller/Steamroller_R.png";
var BUILDING = "CityHall/CityHall_64x64.png";
var NEWSPAPER = "Newspaper.png";

var G2 = g();

class PowerUp {
	var image = "CheckMark.png";
	var position = null;
	var spr = null;
	var scene = null;

	function PowerUp(p, s){
		this.scene = s;
		this.spr = s.addsprite(this.image).pos(G2.tile_to_world(p)).prepare().bringtofront();
		this.position = p;
		trace("PowerUP "+str(this.position));
	}
	function animate_position(){
	}
	function destroy(){
               spr.removefromparent();
       }
}

class Player {
	var image = null;
	var position = null;
	var spr = null;
	var direction = G2.UP;
	var next_direction = G2.UP;
	var action = null;
	var player_speed = 750;
	var scene = null;
	var newspaper_speed = 300;
	var m = null;
	var making_roads = False;

	function Player(i, p, map) {
		this.image = i;
		this.scene = map.scene;
		this.spr = map.scene.addsprite(i).pos(G2.tile_to_world(p));
		this.spr.bringtofront();
		this.spr.prepare();
		this.m = map;
		this.position = p;
		this.iter_play_loop(map);
	}

	function iter_play_loop(map) {
		c_addtimer(player_speed, this.move, [map, this]);
		c_addtimer(100, this.poll_dir, [map, this]);
	}

	function throw_newspaper(d){
		var pos = [this.position[0]+G2.directions[d][0], this.position[1]+G2.directions[d][1]];
		var p = G2.tile_to_world(this.position);
		var new_pos = [p[0] + G2.directions[d][0]*G2.TILE_W, p[1] + G2.directions[d][1]*G2.TILE_H];

		var paper = this.scene.addsprite(NEWSPAPER).pos(p).prepare();

		function suicide(a,b,params){
			var paper = params[0];
			var m = params[1];
			var pos = params[2];
			paper.removefromparent();
			m.deliver_news(pos[0], pos[1]);
		}

		paper.addaction(moveto(newspaper_speed, new_pos[0], new_pos[1]));
		c_addtimer(newspaper_speed, suicide, [paper,m,pos], newspaper_speed, 1);
	}

	function move(timer, tick, params) {
		var that = params[1];
		var m = params[0];

		if(m.check_zombie(that.position)) {
			// die motherfucker die!
		}

		var x = that.position[0];
		var y = that.position[1];
		function if_house_throw(d,x,y,m,that){
			if(m.is_house(x+G2.directions[d][0], y+G2.directions[d][1])){
				that.throw_newspaper(d);
			}
		}
		if(this.direction == G2.RIGHT || this.direction == G2.LEFT){
			if_house_throw(G2.DOWN,x,y,m,this);
			if_house_throw(G2.UP,y,x,m,this);
		}
		if(this.direction == G2.DOWN || this.direction == G2.UP){
			if_house_throw(G2.LEFT,x,y,m,this);
			if_house_throw(G2.RIGHT,x,y,m,this);
		}

		var new_pos = [that.position[0] + G2.directions[that.direction][0], that.position[1] + G2.directions[that.direction][1]];
		
		if(m.check_powerup(new_pos)){
			m.collect_powerup(new_pos);
			that.making_roads = True;
			//c_addtimer(10000, that.powerup_timeout, [m, that], 1);
		}

		if(m.is_road(new_pos[0], new_pos[1])) {
			that.position = new_pos;
		} else if(m.is_grass(new_pos[0], new_pos[1]) && that.making_roads){
                       that.position = new_pos;
                       m.make_road(new_pos[0], new_pos[1]);
                       m.make_roads_good();
               }
		that.animate_position();
	}

	function poll_dir(timer, tick, params) {
		var that = params[1];
		var m = params[0];
		if(that.next_direction != that.direction) {
			var nps = [that.position[0] + G3.directions[that.next_direction][0], that.position[1] + G3.directions[that.next_direction][1]];
			if(m.is_road(nps[0], nps[1]) || that.making_roads) {
				that.direction = that.next_direction;
			}
		}
		var wp = that.spr.pos();
		var start_cam = G3.tile_to_world(that.position);
		m.cam.set(m.cam.view_size[0]/2-wp[0], m.cam.view_size[1]/2-wp[1]);
		m.larrow.pos([wp[0]-m.cam.view_size[0]/2, wp[1]]);
		m.rarrow.pos([wp[0]+m.cam.view_size[0]/2-64, wp[1]]);
		m.uarrow.pos([wp[0], wp[1]-m.cam.view_size[1]/2]);
		m.darrow.pos([wp[0], wp[1]+m.cam.view_size[1]/2-64]);
		m.cam.update(m.scene);
	}



	/*function animate_position() {
		var p = G2.tile_to_world(this.position);
		this.spr.addaction(moveto(player_speed, p[0], p[1]));
		if(this.direction == G2.LEFT) {
			if(this.)
			this.spr.texture(PLAYER_L);
		}
		if(this.direction == G2.RIGHT) {
			this.spr.texture(PLAYER_R);
		}
		if(this.direction == G2.UP) {
			this.spr.texture(PLAYER_U);
		}
		if(this.direction == G2.DOWN) {
			this.spr.texture(PLAYER_D);
		}
	}*/
	function animate_position() {
               var p = G2.tile_to_world(this.position);
               this.spr.addaction(moveto(player_speed, p[0], p[1]));

               if(!this.making_roads){
                       if(this.direction == G2.LEFT) {
                               this.spr.texture(PLAYER_L);
                       }
                       if(this.direction == G2.RIGHT) {
                               this.spr.texture(PLAYER_R);
                       }
                       if(this.direction == G2.UP) {
                               this.spr.texture(PLAYER_U);
                       }
                       if(this.direction == G2.DOWN) {
                               this.spr.texture(PLAYER_D);
                       }
               }


               if(this.making_roads){
               	trace("MAKIN' ROADS!");
                       if(this.direction == G2.LEFT) {
                               this.spr.texture(STEAM_L);
                       }
                       if(this.direction == G2.RIGHT) {
                               this.spr.texture(STEAM_R);
                       }
                       if(this.direction == G2.UP) {
                               this.spr.texture(STEAM_U);
                       }
                       if(this.direction == G2.DOWN) {
                               this.spr.texture(STEAM_D);
                       }
               }
       }
 
	function powerup_timeout(timer, tick, params){
               var that = params[1];
               var m = params[0];
               trace("POWER TO!");
               that.making_roads = False;
       }

}

class House {
	var image = null;
	var position = null;
	var spr = null;
	var scene = null;
	var news = False;

	function House(i, p, sc) {
		this.image = i;
		//this.spr = sprite(i);
		this.position = p;
		this.scene = sc;
		var tpos = [G2.TILE_W*p[0], G2.TILE_H*p[1]];
		spr = sc.addsprite(i).pos(tpos);
		spr.prepare();
	}

	function set_image(i){
		image = i;
		spr.removefromparent();
		var tpos = [G2.TILE_W*position[0], G2.TILE_H*position[1]];
		spr = this.scene.addsprite(i).pos(tpos);
		spr.prepare();
	}

	function animate_position(x,y){
	}
	function destroy(){
               spr.removefromparent();
       }
}