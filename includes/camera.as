class Camera {
	var view_size = null;
	var pos = null;
	var last_touch = null;

	function Camera(size, position) {
		this.view_size = size;
		this.pos = position;
	}

	function zoom(amount) {
		this.view_size[0] += amount;
		this.view_size[1] += amount;
		this.move(amount/2, amount/2);	
	}

	function move(dx, dy) {
		this.pos[0] += dx;
		this.pos[1] += dy;
	}

	function set(dx, dy) {
		this.pos[0] = dx;
		this.pos[1] = dy;
	}

	function update(scene) {
		scene.pos([this.pos[0], this.pos[1]]);
		v_scale(this.view_size[0], this.view_size[1]);
	}

	function untouch_reset() {
		this.last_touch = null;
	}


	function touch_pan(x, y, scene) {
		if(this.last_touch == null) {
			this.last_touch = [x ,y];
		} else {
			var dx = x - this.last_touch[0];
			var dy = y - this.last_touch[1];
			this.move(dx, dy);
			this.update(scene);
		}
	}

	function convert_to_screen(x, y) {
		var hw_screen = screensize();
		var xp = x - this.pos[0];
		var yp = y - this.pos[1];

		var rx = xp*hw_screen[0]/this.view_size[0];
		var ry = yp*hw_screen[1]/this.view_size[1];
		return [rx, ry];
	}

	function get_center() {
		return [this.pos[0]+this.view_size[0]/2, this.pos[1]+this.view_size[1]/2];
	}
}