class g {
	var TILE_W = 64;
	var TILE_H = 64;

	var LEFT = 3
	var RIGHT = 2
	var UP = 1
	var DOWN = 0
	var directions = [[0,1],[0,-1],[1,0],[-1,0]]


	function tile_to_world(p){
	    return [TILE_W * p[0], TILE_H * p[1]];
	}
}