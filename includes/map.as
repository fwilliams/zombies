var TERRAIN_GRASS = "GrassTile/GrassTile_64x64.png";
var BUILDING_CITY_HALL = "CityHall/CityHall_64x64.png";
var BUILDING_HOUSE_U = "HouseBack/House_Back_N.png";
var BUILDING_HOUSE_D = "House1/House_N_64.png";
var BUILDING_HOUSE_S = "HouseSide/House_Side_N.png";
var BUILDING_HOUSE_U_NEWS = "HouseBack/House_Back_Y.png";
var BUILDING_HOUSE_D_NEWS = "House1/House_Y_64.png";
var BUILDING_HOUSE_L_NEWS = "HouseSide/House_LSide_Y.png";
var BUILDING_HOUSE_R_NEWS = "HouseSide/House_RSide_Y.png";
var PLAYER_R = "Bike/Bike_R.png";
var OBJECT_ROAD_H = "Road/Horizontal/Road_Horizontal_64x64.png";
var OBJECT_ROAD_V = "Road/Vertical/Road_Vertical_64x64.png";
var OBJECT_ROAD_TURN_R_U = "Road/Turn/Turn_R_U.png";
var OBJECT_ROAD_TURN_R_D = "Road/Turn/Turn_R_D.png";
var OBJECT_ROAD_TURN_L_U = "Road/Turn/Turn_L_U.png";
var OBJECT_ROAD_TURN_L_D = "Road/Turn/Turn_L_D.png";
var OBJECT_ROAD_3_U = "Road/3Way/3Way_U_64x64.png";
var OBJECT_ROAD_3_D = "Road/3Way/3Way_D_64x64.png";
var OBJECT_ROAD_3_L = "Road/3Way/3Way_L_64x64.png";
var OBJECT_ROAD_3_R = "Road/3Way/3Way_R_64x64.png";
var OBJECT_ROAD_4 = "Road/4Way/4Way_64x64.png";
var ARROW_UP = "ArrowU.png";
var ARROW_DOWN = "ArrowD.png";
var ARROW_LEFT = "ArrowL.png";
var ARROW_RIGHT = "ArrowR.png";

var G3 = g();

class Map {
	var map = [];
	var width = 0;
	var height = 0;
	var tile_size;
	var scene = getscene();
	var cam = null;
	var zombies = [];
	var player = null;
	var larrow = null;
	var rarrow = null;
	var uarrow = null;
	var darrow = null;

	function Map(w, h, tile_width, tile_height) {
		this.width = w;
		this.height = h; 
		this.tile_size = [tile_width, tile_height];
		this.init_map();
		this.cam = Camera(screensize(), [0, 0]);
		this.cam.update(this.scene);
		this.init_game();
		this.init_event_handlers();
	}

	function init_event_handlers() {
		this.scene.setevent(EVENT_UNTOUCH, untouch_event_handler);
		this.uarrow.setevent(EVENT_TOUCH, this.set_dir_up);
		this.darrow.setevent(EVENT_TOUCH, this.set_dir_down);
		this.larrow.setevent(EVENT_TOUCH, this.set_dir_left);
		this.rarrow.setevent(EVENT_TOUCH, this.set_dir_right);

	}
	function set_dir_up(node, event, params, x, y, points) {
		this.player.next_direction = G3.UP;
		vibrate(50);
	}
	function set_dir_right(node, event, params, x, y, points) {
		this.player.next_direction = G3.RIGHT;
		vibrate(50);
	}
	function set_dir_left(node, event, params, x, y, points) {
		this.player.next_direction = G3.LEFT;
		vibrate(50);
	}
	function set_dir_down(node, event, params, x, y, points) {
		this.player.next_direction = G3.DOWN;
		vibrate(50);
	}

	function init_game() {
		var start_tile = [this.width/2, this.height/2];
		var start_cam = G3.tile_to_world(start_tile);

		trace(start_cam);
		this.player = Player(PLAYER_R, start_tile, this);

		this.larrow = this.scene.addsprite(ARROW_LEFT);
		this.larrow.pos([start_cam[0]-this.cam.view_size[0]/2, start_cam[1]]);
		this.larrow.prepare();
		this.larrow.bringtofront();

		this.rarrow = this.scene.addsprite(ARROW_RIGHT);
		this.rarrow.pos([start_cam[0]+this.cam.view_size[0]/2-64, start_cam[1]]);
		this.rarrow.prepare();
		this.rarrow.bringtofront();

		this.uarrow = this.scene.addsprite(ARROW_UP);
		this.uarrow.pos([start_cam[0], start_cam[1]-this.cam.view_size[1]/2]);
		this.uarrow.prepare();
		this.uarrow.bringtofront();

		this.darrow = this.scene.addsprite(ARROW_DOWN);
		this.darrow.pos([start_cam[0], start_cam[1]+this.cam.view_size[1]/2-64]);
		this.darrow.prepare();
		this.darrow.bringtofront();

		this.add(player);
	}

	function init_map() {
		for(var i=0; i<this.width; i++) {
			map.append([]);
			for(var j=0; j<this.height; j++) {
				var tpos = [this.tile_size[0]*i, this.tile_size[1]*j];
				var spr = this.scene.addsprite(TERRAIN_GRASS).pos(tpos);
				spr.prepare();
				this.map[i].append(Tile(spr, i, j,scene));
			}
		}
		this.scene.size([this.width*this.tile_size[0], this.height*this.tile_size[1]]);
	}

	function add(object) {
		var x = object.position[0];
		var y = object.position[1];
		this.map[x][y].add(object);
	}

	function remove(object) {
		var x = object.position[0];
		var y = object.position[1];
		this.map[x][y].remove(object);
	}

	function make_roads_good(){
		for(var y=0; y<this.height; y++) {
			for(var x=0; x<this.width; x++) {
				if(map[x][y].is_road()){
					var roadType = get_roadtype(x,y);
					if(roadType){
						map[x][y].set_road_sprite(roadType);
					}
				}
			}
		}
	}

	function check_zombie(pos) {
		for(var i=0; i<len(this.zombies); i++) {
			if(pos == this.zombies[i]) {
				return True;
			}
			return False;
		}
	}

	function make_houses_good(){
		var newsCount = 0;
		var houseCount = 0;
		for(var y=0; y<this.height; y++) {
			for(var x=0; x<this.width; x++) {
				if(map[x][y].has(House)){
					var news = map[x][y].get(House).news;
					if(news){
						newsCount++;
					}
					houseCount++;
					var houseType = get_housetype(x,y,news);
					if(houseType){
						map[x][y].get(House).set_image(houseType);
					}
				}
			}
		}
	}

	function deliver_news(x,y){
		if(map[x][y].has(House)){
			map[x][y].get(House).news = True;
		}
		this.make_houses_good();
	}

	function get_housetype(x,y,news){
		var house = BUILDING_HOUSE_D;
		if (x>= 0 && y >= 0 && x < width && y < height && map[x][y].has(House)){
			var top = can_path_up(x,y);
			var bottom = can_path_down(x,y);
			var left = can_path_left(x,y);
			var right = can_path_right(x,y);
			
			if(left){
				if(news){house = BUILDING_HOUSE_L_NEWS;}
				else{house = BUILDING_HOUSE_S;}
			}
			if(right){
				if(news){house = BUILDING_HOUSE_R_NEWS;}
				else{house = BUILDING_HOUSE_S;}
			}
			if(top){
				if(news){house = BUILDING_HOUSE_U_NEWS;}
				else{house = BUILDING_HOUSE_U;}
			}
			if(bottom){
				if(news){house = BUILDING_HOUSE_D_NEWS;}
				else{house = BUILDING_HOUSE_D;}
			}
		}
		return house;
	}

	function make_road(x,y){
		map[x][y].set_terrain("Road");
	}

	function is_road(x,y){
		if(x < 0 || y < 0 || x >= this.width || y >= this.height) {
			return False;
		}
		return map[x][y].is_road();
	}

	function is_house(x,y){
		if(x < 0 || y < 0 || x >= this.width || y >= this.height) {
			return False;
		}
		return map[x][y].has(House);
	}

	function make_house(x,y,h){
		map[x][y].add_house(h);
	}

	function touch_event_handler(node, event, params, x, y, points) {
		trace([x, y]);
		var region = this.get_region(x, y);

		this.player.next_direction = region;
		trace("REGION: " + str(region));
		//this.cam.touch_pan(x, y, this.scene);
	}

	function untouch_event_handler(node, event, params, x, y, points) {
		this.cam.untouch_reset();
	}

	function get_region(x, y) {
		var scr = screensize();

		var s = this.cam.convert_to_screen(x, y);
		var ss =[0, 0];
		ss[0] = s[0]*scr[0]/scr[1];
		ss[1] = s[1]*scr[1]/scr[0];

		if(ss[0]<ss[1] && s[0]<scr[0]/2) {
			return G3.LEFT;
		}
		if(ss[0]>ss[1] && s[0]>scr[0]/2) {
			return G3.RIGHT;
		}
		if(ss[1]<ss[0] && s[1]<scr[1]/2) {
			return G3.UP;
		}
		if(ss[1]>ss[0] && s[0]>scr[1]/2) {
			return G3.DOWN;
		}

	}

	function get_roadtype(x, y){
		var road = null;
		if (x>= 0 && y >= 0 && x < width && y < height && map[x][y].is_road()){
			var top = can_path_up(x,y);
			var bottom = can_path_down(x,y);
			var left = can_path_left(x,y);
			var right = can_path_right(x,y);
			
			if (top || bottom){road = OBJECT_ROAD_V;}
			if (left || right){road = OBJECT_ROAD_H;}
			if (left && top){road = OBJECT_ROAD_TURN_L_U;}
			if (left && bottom){road = OBJECT_ROAD_TURN_L_D;}
			if (right && top){road = OBJECT_ROAD_TURN_R_U;}
			if (right && bottom){road = OBJECT_ROAD_TURN_R_D;}
			if (left && right && top){road = OBJECT_ROAD_3_U;}
			if (left && right && bottom){road = OBJECT_ROAD_3_D;}
			if (left && top && bottom){road = OBJECT_ROAD_3_L;}
			if (right && top && bottom){road = OBJECT_ROAD_3_R;}
			if (left && right && top && bottom){road = OBJECT_ROAD_4;}
		}
		return road;
	}

	function can_path_up(x, y){
		return y > 0 && map[x][y-1].is_road();
	}

	function can_path_down(x, y){
		return y < width-1 && map[x][y+1].is_road();
	}

	function can_path_left(x, y){
		return x > 0 && map[x-1][y].is_road();
	}

	function can_path_right(x, y){
		return x < width-1 && map[x+1][y].is_road();
	}

	function debug_print() {
		for(var y=0; y<this.height; y++) {
			var s = "";
			for(var x=0; x<this.width; x++) {
				s += this.map[x][y].toString();
			}
		}
	}
function collect_powerup(pos){
               var x = pos[0];
               var y = pos[1];
               trace("EREWZRESREWARE");
               map[x][y].remove_all();//removes powerup
       }
function check_powerup(pos){
               var x = pos[0];
               var y = pos[1];
               if(x < 0 || y < 0 || x >= this.width || y >= this.height) {
                       return False;
               }
               return map[x][y].has(PowerUp);
       }
function is_grass(x,y){
               if(x < 0 || y < 0 || x >= this.width || y >= this.height) {
                       return False;
               }
               return True;
       }
}

class Tile {
	var spr = null;
	var terrain = "Grass";
	var objects = [];
	var position = null;
	var scene = null;

	function Tile(s, x, y, sc) {
		this.spr = s;
		this.position = [x, y];
		this.scene = sc;
	}

	function toString() {
		if (is_occupied() && is_road()) {
			return "R";
		} 
		else if(is_occupied()){
			return "H";
		}
		else {
			return " ";
		}
	}

	function add(object) {
		this.objects.append(object);
	}

	function add_house(object) {
		this.objects.append(object);
		//spr = spr.addsprite(object.image);
	}

	function set_road_sprite(road){
		//spr = sprite(road);

		var tpos = [G3.TILE_W*position[0], G3.TILE_H*position[1]];
		spr = this.scene.addsprite(road).pos(tpos);
		spr.prepare();
	}

	function is_occupied() {
		return len(objects) != 0;
	}

	//check if it has Road, Player, etc.
	function has(klass) {
		for(var i = 0; i < len(this.objects); i++){
			if(type(this.objects[i]) == klass){
				return True;
			}
		}
		return False;
	}

	function is_road(){
		return terrain == "Road";
	}

	function set_terrain(t){
		terrain = t;
	}

	function get(klass) {
		for(var i = 0; i < len(this.objects); i++){
			if(type(this.objects[i]) == klass){
				return this.objects[i];
			}
		}
		return null;
	}

function remove_all() {
               for(var i = 0; i < len(this.objects); i++){
                       this.objects[i].destroy();
               }
               this.objects.clear();
       }
}