import includes.globals;
var G = g();
import includes.gameobject;
import includes.camera;
import includes.map;

var map = Map(20, 20, G.TILE_W, G.TILE_H);

//draw a road throug the middle
var middle = map.height / 2;
for(var i=0; i<map.width; i++) {
    map.make_road(i, middle);
}
var grid_w = 4;
var grid_h = 4;
//adds a road if possible with a length, co-ordinates and direction
function add_road(length, x, y, d){
    for (var c=0; c<length; c++){
        var pos = [x + G.directions[d][0]*c, y + G.directions[d][1]*c];
        if(pos[0] >= 0, pos[1] >= 0, pos[0] < map.width, pos[1] < map.height){
            map.make_road(pos[0], pos[1]);
            if(rand(30) == 0){
                map.add(PowerUp(pos, map.scene));
            }
        }
    }
}
for(i=0; i<map.width; i+=grid_w) {
    for(var j=0; j<map.height; j+=grid_h) {
        if(rand(3) > 0){
            add_road(grid_h+1, i, j, G.DOWN);
        }
        if(rand(3) > 0){
            add_road(grid_h+1, i, j, G.RIGHT);
        }
    }
}

function addHouses(map){
	for(var x=1; x<map.width-1; x++) {
	    for(var y=1; y<map.height-1; y++) {
	    	if(!map.is_road(x,y)){
	    		//trace("check house ");
	    		if(map.is_road(x-1,y) || map.is_road(x+1,y) || map.is_road(x,y-1) || map.is_road(x,y+1)){
	    			//trace("maybe house ");
	    			if(rand(3)==0){
	    				var pos = [x,y];
	    				//trace("*** make house "+ str(x) + " " + str(y) + " ***");
	    				map.make_house(x,y,new House("CityHall/CityHall_64x64.png", pos, map.scene));
	    			}
	    			//else{
	    			//	trace("maybe not");
	    			//}
	    		}
	    	}
	    }
	}
}


map.make_roads_good();
addHouses(map);
map.make_houses_good();
map.debug_print();


function shut_down_everything(){
    quitgame();
}

var m1 = newmenu("Close", shut_down_everything, null);
setmenu(m1);
